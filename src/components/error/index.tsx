import React, { FC } from 'react';

import styles from './styles.css';

interface IError {
	message: string;
}

export const Error: FC<IError> = ({ message }) => {
	return <div className={styles.error}>{message}</div>;
};
