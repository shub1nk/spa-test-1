import React, { FC, ButtonHTMLAttributes } from 'react';

import styles from './styles.css';

interface IButton extends ButtonHTMLAttributes<HTMLButtonElement> {}

export const Button: FC<IButton> = (props) => {
	return (
		<div className={styles.wrapper}>
			<button className={styles.button} {...props}>
				{props.children}
			</button>
		</div>
	);
};
