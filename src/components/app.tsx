import React, { FC } from 'react';

import { Login } from '@components/login';

export const App: FC = () => {
	return (
		<section>
			<Login />
		</section>
	);
};
