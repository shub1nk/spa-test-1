import React, { ChangeEvent, FC, FormEvent, useState } from 'react';
import axios from 'axios';

import { Input } from '@components/input';
import { Button } from '@components/button';
import { Error } from '@components/error';
import { SERVICE_URL } from '../../constants';

import styles from './styles.css';

export const Login: FC = () => {
	const [username, setUserName] = useState('');
	const [password, setPassword] = useState('');
	const [error, setError] = useState('');

	const isDisabled = !username || !password;

	const handleChange = (cb) => (e: ChangeEvent<HTMLInputElement>) => {
		cb(e.target.value);
	};

	const handleSubmit = (e: FormEvent) => {
		e.preventDefault();

		axios
			.post(SERVICE_URL, {
				username,
				password,
			})
			.then((response) => {
				console.log('response');
				console.log(response);
			})
			.catch((error) => {
				console.log('error', error.response.data.description);
				setError(error.response.data.description);
			});
	};

	return (
		<form className={styles.form} onSubmit={handleSubmit}>
			<Input
				type="text"
				name="username"
				value={username}
				label="Username"
				id="username"
				onChange={handleChange(setUserName)}
			/>
			<Input
				type="password"
				name="password"
				value={password}
				label="Password"
				id="password"
				onChange={handleChange(setPassword)}
			/>
			<Error message={error} />
			<Button type="submit" disabled={isDisabled}>
				Login
			</Button>
		</form>
	);
};
