import React, { FC, ChangeEvent } from 'react';

import styles from './styles.css';

interface IInputField {
	type: 'text' | 'password';
	name: string;
	id: string;
	value: string;
	label: string;
	onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

export const Input: FC<IInputField> = ({
	label,
	type,
	name,
	id,
	value,
	onChange,
}) => {
	return (
		<div className={styles.wrapper}>
			<div className={styles.label}>
				<label htmlFor={id}>{label}</label>
			</div>
			<div>
				<input
					className={styles.input}
					type={type}
					name={name}
					id={id}
					value={value}
					onChange={onChange}
					autoComplete="off"
				/>
			</div>
		</div>
	);
};
